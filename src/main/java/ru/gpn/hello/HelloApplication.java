package ru.gpn.hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@SpringBootApplication
public class HelloApplication {
    @GetMapping(path = "/")
    public String index() {
        return "external" ;
    }
    public static void main(String[] args) {
        SpringApplication.run(HelloApplication.class, args);
    }

}
